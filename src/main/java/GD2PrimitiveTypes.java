public class GD2PrimitiveTypes
{
    public static void main(String[] args)
    {
        playWithInteger();
        playWithByte();
        showJavaUsesIntAsDefault();
        playWithFloats();
        playWithDoubles();
        playWithChars();
        bitShiftTesting();
        extractBitsFromByte();
    }

    private static void extractBitsFromByte()
    {
        byte soundsInGame = 35;
        checkSirenSound(soundsInGame);
    }

    private static void checkSound(byte soundsInGame)
    {
        byte sirenChecker = 1;
        final int sirenBitPosition = 3;
        sirenChecker = (byte)(sirenChecker << sirenBitPosition);
        byte andResult = (byte)(sirenChecker & soundsInGame);
        int onOrOff = andResult >> sirenBitPosition;
        System.out.println(onOrOff);
        boolean sirenOn;
        if(onOrOff == 1)
        {
            sirenOn = true;
        }
        else
        {
            sirenOn = false;
        }
    }

    private static void bitShiftTesting()
    {
        int one = 1;
        int oneTimesSeven = one << 7;
        System.out.println(oneTimesSeven);
    }

    private static void playWithChars()
    {
        char input = 'a';
        System.out.println(input);
    }

    private static void playWithFloats()
    {
        System.out.println("A float is " + Float.SIZE + " bits");
        float maxFloat = Float.MAX_VALUE;
        float minFloat = Float.MIN_VALUE;
        System.out.println("Max float is " + maxFloat + " , min float is " + minFloat);
        float averageGD2Mark = 52.678f;
    }

    private static void playWithDoubles()
    {
        System.out.println("A double is " + Double.SIZE + " bits");
        double maxDouble = Double.MAX_VALUE;
        double minDouble = Double.MIN_VALUE;
        System.out.println("Max double is " + maxDouble + " , min double is " + minDouble);
        double averageGD2Mark = 52.678f;
    }

    private static void showJavaUsesIntAsDefault()
    {
        byte smallNumber = 100;
        byte otherSmallNumber = (byte)(smallNumber/2);
    }

    public static void playWithInteger()
    {
        System.out.println("An integer is " + Integer.SIZE + " bits");
        int maxInt = Integer.MAX_VALUE;
        int minInt = Integer.MIN_VALUE;
        System.out.println("Max int is " + maxInt + " , min int is " + minInt);
    }

    public static void playWithByte()
    {
        System.out.println("An byte is " + Byte.SIZE + " bits");
        int maxByte = Byte.MAX_VALUE;
        int minByte = Byte.MIN_VALUE;
        System.out.println("Max byte is " + maxByte + " , min byte is " + minByte);
    }
}
